const path = require('path');

module.exports = [
    // Main production bundle
    {
        mode: 'production'
    },

    // Testing bundle
    {
        mode: 'development',
        entry: './test/index.js',
        output: {
            path: path.resolve(__dirname, 'test', 'browser'),
            filename: 'browser.js'
        }
    }
];
