browser-generate-password
=========================
A simple function to generate a random password on demand. It uses `window.crypto`
when available to help make it _more randomer_.


Usage
=====
Install with: `npm install browser-generate-password`

Then use in your project:
```js
import genPassword from 'browser-generate-password';

const password = genPassword(15); // 0{9gSE)Mc9Pj1Ja
```


Testing
=======
A full test suite is included. To run it:

1. Clone the repository, and `cd` into it
2. Install dev dependencies: `npm install`
3. Run node.js tests: `npm test`
4. Build browser bundle: `npm run build`
5. Run browser tests by opening `test/browser/index.html` in your browser
   of choice.

The browser based test suite also includes a histogram showing character
distribution in the generated passwords. It's a nice way to see how uniform it
comes out in your browser.

You will see the numeric characters skew higher in the histogram because the
function ensures there is at least one number in the generated password. This
isn't really ideal, but since so many websites require a number, it makes sense
to do it.

![Test results example](test-screenshot.png)
