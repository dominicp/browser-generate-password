/**
 * A basic test suite for the functions. Since this involves randomness
 * we run the functions multiple times and test all results.
 */
import { expect } from 'chai';
import genPassword, { randArray, getDictionary } from '../src/index';

describe('browser-generate-password', function () {

    const sampleSize = 20;

    it('should rock', function () { return true; });

    describe('randArray', function () {

        const getSample = (length, min = 0, max = 5) => new Array(sampleSize).fill(0).map(() => randArray(length, min, max));

        it('should generate an array of the requested length', function () {

            const length = 5;
            const arrays = getSample(length);

            arrays.forEach((array) => {

                expect(array).to.have.lengthOf(length);
            });
        });

        it('should include only numbers in the given range', function () {

            const min = 0;
            const max = 20;
            const arrays = getSample(15, min, max);

            arrays.forEach((array) => {

                array.forEach((number) => {

                    expect(number).to.be.below(max + 1).and.be.above(min - 1);
                });
            });
        });
    });

    describe('genPassword', function () {

        const getSample = (length, includeSpecials = true, count = sampleSize) => new Array(count).fill(0).map(
            () => genPassword(length, includeSpecials)
        );

        it('should return a string of the given length', function () {

            const length = 15;
            const passwords = getSample(length);

            passwords.forEach((password) => {

                expect(password.length).to.equal(length);
            });
        });

        it('should include at least one number', function () {

            const passwords = getSample(8);

            passwords.forEach((password) => {

                expect(password.replace(/[^0-9]/g, '')).to.have.lengthOf.above(0);
            });
        });

        it('should not include specials if not requested', function () {

            const passwords = getSample(20, false);

            passwords.forEach((password) => {

                expect(password.replace(/[A-Za-z0-9]/g, '')).to.have.lengthOf(0);
            });
        });

        it('should use all characters in the dictionary evenly', function () {

            // Helper function get the frequency of all of the characters in the given string
            const getDistribution = (str, initialDistribution) => str.split('').reduce((acc, char) => {

                acc[char] = (typeof acc[char] === 'undefined') ? 0 : (acc[char] + 1);

                return acc;

            }, initialDistribution);

            // Helper function to get standard deviation
            const average = data => data.reduce((sum, value) => sum + value) / data.length;
            const stdDev = values => Math.sqrt(average(values.map(value => (value - average(values)) ** 2)));

            // Get the dictionary
            const dictionary = getDictionary(true);
            const dictLength = dictionary.length;

            // Generate a bunch of passwords
            const passLength = 15;
            const passCount = 1000;
            const passwords = getSample(passLength, true, passCount).join('');

            // Get the frequency of all characters in the generated passwords
            const expectedChars = getDistribution(dictionary, {});
            const distribution = getDistribution(passwords, expectedChars);
            const frequencies = Object.values(distribution);
            const stdDeviation = stdDev(frequencies);
            const expectedFreq = ((passLength * passCount) / dictLength);

            // If the test is being run in a browser, plot a histogram
            if (typeof window !== 'undefined' && typeof window.plotHistogram === 'function') {

                window.plotHistogram(distribution, expectedFreq);
            }

            // Run some tests on the distribution
            // Note that the expected values tested here are pretty arbitrary. Our data doesn't come out
            // uniform due to the logic that ensures there is at least one number in the generated
            // password. This is unfortunate, but since many websites require at least one number it
            // can't really be helped.
            expect(Math.min(...frequencies)).to.be.above(expectedFreq * 0.62);
            expect(Math.max(...frequencies)).to.be.below(expectedFreq * 1.37);
            expect(stdDeviation).to.be.lessThan(18.5);
        });
    });
});
